FROM golang:1.17.7-alpine3.15 AS builder

RUN apk add --no-cache git=2.34.1-r0 \
 && git clone https://gitlab.com/orobardet/gitlab-ci-linter.git

WORKDIR /go/gitlab-ci-linter

RUN CGO_ENABLED=0 go install -ldflags '-extldflags "-static"' -tags timetzdata

FROM scratch

COPY --from=builder /go/bin/gitlab-ci-linter /bin/gitlab-ci-linter
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

WORKDIR /data

ENTRYPOINT ["/bin/gitlab-ci-linter"]